<?php
/**
 * Copyright © 2017 seitenwind
 */

if (!defined('ABSPATH')) {
    die();
}

global $avia_config;

/*
 * get_header is a basic wordpress function, used to retrieve the header.php file in your theme directory.
 */
get_header();


echo avia_title(array('title' => __('Error 404 - page not found', 'avia_framework')));

do_action('ava_after_main_title');
?>

<div class='container_wrap container_wrap_first alternate_color <?php avia_layout_class('main'); ?>'>

    <?php
    do_action('avia_404_extra'); // allows user to hook into 404 page fr extra functionallity. eg: send mail that page is missing, output additional information
    ?>

    <div class='container'>

        <main class='template-page content <?php avia_layout_class('content'); ?> units' <?php avia_markup_helper(array('context' => 'content')); ?>>

            <div class="entry entry-content-wrapper clearfix" id='search-fail'>
                <div class="e404-content">
                    <p class="error-message">404 | Seite nicht gefunden</p>
                    <h1>Entschuldigung</h1>
                    <h2 class="line-after">Wir können die gewünschte Seite leider nicht finden</h2>
                    <p class="e404-details">
                        Ein Grund hierfür könnte sein, dass Sie eine falsche oder veraltete URL aufgerufen haben oder wir die betreffende Seite archiviert, verschoben oder umbenannt haben. </p>
                    <p class="e404-details">
                        Werfen Sie doch einen Blick auf unser umfangreiches Sortiment und die aktuellen Angebote, da ist sicher das Richtige für Sie dabei. </p>

                    <div class="flex_column av_one_third  flex_column_div av-zero-column-padding first  avia-builder-el-10  el_after_av_textblock  el_before_av_one_third  column-top-margin" style="border-radius:0px; ">
                        <span class="av_font_icon avia_animate_when_visible av-icon-style-border  avia-icon-pos-center  avia_start_animation avia_start_delayed_animation" style="color:#b6c700;border-color:#b6c700;"><a href="http://www.hans-wagner.de/sortiment/food-sortiment/" class="av-icon-char" style="font-size:80px;line-height:80px;width:80px;" aria-hidden="true" data-av_icon="" data-av_iconfont="hanswagner-fontello-2"></a><span class="av_icon_caption av-special-font">Food Sortiment</span></span>
                    </div>

                    <div class="flex_column av_one_third  flex_column_div av-zero-column-padding   avia-builder-el-13  el_after_av_one_third  el_before_av_one_third  column-top-margin" style="border-radius:0px; ">
                        <span class="av_font_icon avia_animate_when_visible av-icon-style-border  avia-icon-pos-center  avia_start_animation avia_start_delayed_animation" style="color:#f7a600; border-color:#f7a600;"><a href="http://www.hans-wagner.de/sortiment/non-food-sortiment/" class="av-icon-char" style="font-size:80px;line-height:80px;width:80px;" aria-hidden="true" data-av_icon="" data-av_iconfont="hanswagner-fontello-2"></a><span class="av_icon_caption av-special-font">Non-Food Sortiment</span></span>
                    </div>

                    <div class="flex_column av_one_third  flex_column_div av-zero-column-padding   avia-builder-el-16  el_after_av_one_third  avia-builder-el-last  column-top-margin" style="border-radius:0px; ">
                        <span class="av_font_icon avia_animate_when_visible av-icon-style-border  avia-icon-pos-center  avia_start_animation avia_start_delayed_animation" style="color:#ea4f44; border-color:#ea4f44;"><a href="http://www.hans-wagner.de/sortiment/maschinen-und-technik/" class="av-icon-char" style="font-size:80px;line-height:80px;width:80px;" aria-hidden="true" data-av_icon="" data-av_iconfont="hanswagner-fontello"></a><span class="av_icon_caption av-special-font">Maschinen und Technik</span></span>
                    </div>

                    <hr>

                    <p class="e404-details">
                        Oder nutzen Sie nachfolgende Links, um die gewünschten Informationen zu erhalten:
                    </p>

                    <p class="e404-details">
                        Gerne sind wir Ihnen auch direkt behilflich. Schreiben Sie uns eine <a href="mailto:info@hans-wagner.de">E-Mail</a> oder rufen Sie uns an: <br>
                        <a href="tel:+49962147540">09621 4754-0</a>
                    </p>

                </div>
            </div>

        </main>

    </div>

</div>


<?php get_footer(); ?>
