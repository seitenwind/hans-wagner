/*
 * Copyright © 2017 seitenwind
 */

var gulp = require("gulp");
var compass = require("gulp-compass");
var minifyCss = require("gulp-minify-css");
var rename = require("gulp-rename");
var tsc = require("gulp-typescript");
var uglify = require("gulp-uglify");

var tsProject = tsc.createProject('typescript/tsconfig.json');

function swallowError(error) {
    console.log(error.toString());
    this.emit('end')
}

gulp.task("default", function () {
    gulp.watch('scss/**/*.scss', ['styles']);
    gulp.watch('typescript/**/*.ts', ['scripts']);
});

gulp.task("styles", function () {
    gulp.src(['scss/style.scss', 'scss/admin.scss'])
        .pipe(compass({
            config_file: 'config.rb',
            sass: "scss/",
            css: "css/"
        }))
        .on("error", swallowError)
        .pipe(gulp.dest("css"))
        .pipe(rename({suffix: ".min"}))
        .pipe(minifyCss())
        .pipe(gulp.dest("css"));
});

gulp.task("scripts", function () {
    gulp.src(['typescript/**/*.ts'])
        .pipe(tsProject())
        .on("error", swallowError)
        .js
        .pipe(gulp.dest("js"))
        .pipe(rename({suffix: ".min"}))
        .pipe(uglify())
        .pipe(gulp.dest("js"));
});