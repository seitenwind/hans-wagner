module Ellipsis
{
	const TEXT_LENGTH_CLASS:string = ".ellipse-length-";
	const DEFAULT_LENGTH:number = 150;
	const DEFAULT_TOLERANCE:number = 50;
	const APPEND = "";

	export const MODE_TOKENS = 0;
	export const MODE_WORDS = 1;
	export const MODE_SENTENCES = 2;

	let $ = jQuery;

	export class EllipsedTextElement
	{
		private $root: JQuery;
		private length: number;
		private tolerance: number;
		private text: string;
		private mode:number;

		constructor($root: JQuery, length: number = DEFAULT_LENGTH, tolerance: number = DEFAULT_TOLERANCE)
		{
			this.$root = $root;
			this.tolerance = tolerance;
			this.length = this.extractTextLength(length);
			this.text = $root.text();
			this.setMode(MODE_TOKENS);
		}

		/**
		 * Sets the mode of the ellipse class
		 *
		 * possible values:
		 * 0 MODE_TOKENS - Split everything at token level
		 * 1 MODE_WORDS - Split everything at word level
		 * 2 MODE_SENTENCES - Split everything at sentence level (uses a tolerance to include slightly longer sentences)
		 * @param mode The mode
		 */
		public setMode(mode:number): void
		{
			this.mode = mode;
		}

		private extractTextLength(defaultLength: number): number
		{
			let classes = this.$root.attr("class").split(" ");
			for(let cls of classes)
			{
				if(cls.substr(0, TEXT_LENGTH_CLASS.length) == TEXT_LENGTH_CLASS)
				{
					return parseInt(cls.replace(TEXT_LENGTH_CLASS, ""));
				}
			}
			return defaultLength;
		}

		public apply(): void
		{
			return;
		}

		/**
		 * Split at token level
		 */
		private applyTokens(): void
		{
			let text = this.text.substr(0, this.length);
			text += " …";
			this.$root.empty();
			this.$root.text(text);
		}

		/**
		 * Split at word level
		 */
		private applyWholeWords(): void
		{
			let words = this.text.split(" ");
			let currentLength = 0;
			let text = "";
			for(let word of words)
			{
				currentLength += word.length + 1;
				text += word + " ";

				if(currentLength >= this.length) {
					break;
				}
			}
			text = text.trim();
			text += "…";
			this.$root.empty();
			this.$root.text(text);
		}

		/**
		 * Split at sentence level
		 */
		private applyWholeSentences(): void
		{
			let regex = /[\.\?!]/g;
			let sentences = this.text.split(regex);
			let punctuation = this.text.match(regex);

			let text = "";
			let textFinished = false;
			let self = this;
			sentences.forEach(function (sentence:string, index:number)
			{
				if(textFinished == true) return;

				let punct = "";
				if(punctuation != null)
				{
					punct = punctuation[index];
				}

				sentence = sentence.trim() + punct;
				if ((text.length + sentence.length) <= (self.length + self.tolerance))
				{
					text += sentence;
				}
				else
				{
					textFinished = true;
					return;
				}
			});

			text = text.trim();
			text += APPEND;

			this.$root.empty();
			this.$root.text(text);
		}
	}

	$.fn.ellipse = function(length:number)
	{
		if(length == null || length == 0 || length == undefined) {
			length = DEFAULT_LENGTH;
		}
		new EllipsedTextElement(this, length);
	}

}
