/**
 * resizes icon-boxes so that their height is equal among all boxes in a row.
 *
 * Uses hidden elements to improve rendering/calculation speed.
 */
module IconBox
{
	let $ = jQuery;
	let iconBoxElements: IconBoxElement[];

	function init(): void
	{
		iconBoxElements = [];
		$(".iconbox").each(function (index: number, el: Element)
		{
			iconBoxElements.push(new IconBoxElement($(el)));
		});

		$(window).on("resize", onResize);
		onResize();
	}

	/**
	 * Applies resizing of content and header heights at window resize
	 */
	function onResize(): void
	{
		let rows:IconBoxElement[][] = calculateRows();

		for (let row of rows)
		{
			let maxContentHeight: number = 0;
			let maxHeadlineHeight: number = 0;

			// calculate the maximum content and headline height
			for (let box of row)
			{
				box.onResize();
				let contentHeight = box.getContentHeight();
				let headlineHeight = box.getHeadlineHeight();

				if (contentHeight > maxContentHeight)
				{
					maxContentHeight = contentHeight;
				}

				if (headlineHeight > maxHeadlineHeight)
				{
					maxHeadlineHeight = headlineHeight;
				}
			}

			// apply calculated maximum content and headline height
			for (let box of row)
			{
				box.setContentHeight(maxContentHeight);
				box.setHeadlineHeight(maxHeadlineHeight);
			}
		}
	}

	/**
	 * Returns a list of rows containing the icon boxes. Rows are calculated using the boxes "offsetTop".
	 *
	 * @returns {IconBoxElement[][]} The rows containing the box elements.
	 */
	export function calculateRows(): IconBoxElement[][]
	{
		let rows:Object = {};
		let rowIndices:number[] = [];
		for (let box of iconBoxElements)
		{
			let offset = box.offsetTop();
			if (rows[offset] == undefined || rows[offset] == null)
			{
				rows[offset] = [];
			}
			rows[offset].push(box);
			if(rowIndices.indexOf(offset) == -1) {
				rowIndices.push(offset);
			}
		}
		let result:IconBoxElement[][] = [];
		for (let index of rowIndices)
		{
			result.push(rows[index]);
		}
		return result;
	}

	function createClone($element: JQuery): JQuery
	{
		let $clone: JQuery = $element.clone();
		$clone.addClass("hidden-clone");
		$clone.css("font-size", $element.css("font-size"));
		$clone.css("font-family", $element.css("font-family"));
		$clone.css("font-weight", $element.css("font-weight"));
		$clone.css("letter-spacing", $element.css("letter-spacing"));
		$clone.css("line-height", $element.css("line-height"));
		$("body").append($clone);
		return $clone;
	}

	/**
	 * Represents an icon box element
	 */
	class IconBoxElement
	{
		private $root: JQuery;

		private $headline: JQuery;
		private $content: JQuery;

		private $contentClone: JQuery;
		private $headlineClone: JQuery;

		constructor($root: JQuery)
		{
			this.$root = $root;
			this.$headline = $root.find(".iconbox_content_title");
			this.$content = $root.find(".iconbox_content_container p").last();

			this.$contentClone = createClone(this.$content);
			this.$headlineClone = createClone(this.$headline);

			this.$root.parent().addClass("iconbox-column");
		}

		/**
		 * Returns the top offset of this element
		 *
		 * @returns {number} The offset
		 */
		public offsetTop(): number
		{
			return this.$root.offset().top;
		}

		/**
		 * Handles the resize event by applying the width changes to hidden clones
		 * for height calculation.
		 */
		public onResize(): void
		{
			this.$contentClone.width(this.$content.width());
			this.$headlineClone.width(this.$headline.width());
		}

		/**
		 * Changes the height of the headline
		 * @param height The height that is applied
		 */
		public setHeadlineHeight(height: number): void
		{
			this.$headline.css("min-height", height);
		}

		/**
		 * Returns the calculated height of the headline
		 * @returns {number} The height of the headline
		 */
		public getHeadlineHeight(): number
		{
			return this.$headlineClone.height();
		}

		/**
		 * Changes the height of the content
		 * @param height The height that is applied
		 */
		public setContentHeight(height: number): void
		{
			this.$content.css("min-height", height);
		}

		/**
		 * Returns the calculated height of the headline
		 * @returns {number} The height of the headline
		 */
		public getContentHeight(): number
		{
			return this.$contentClone.height();
		}
	}

	$(init);
}