module Hyphenate
{
	let $: JQueryStatic = jQuery;
	let hyphenator:Hyphenator = null;

	function init(): void
	{
		hyphenator = createHyphenator(hyphenationPatternsDe);
		$(".post-format-icon.minor-meta").remove();
		$("h1, h2, h3, h4, h5, h6").each(function (index: number, el: Element)
		{
			applyHyphenation($(el));
		});
	}

	export function applyHyphenation($el:JQuery): void
	{
		if($el.children().length > 0)
		{
			$el.children().each(function (index: number, el: Element)
			{
				applyHyphenation($(el));
			});
			return;
		}
		$el.text(hyphenate($el.text()));
	}

	function hyphenate(text:string):string
	{
		return text.split(" ").map(function (word: string, index: number)
		{
			if(HyphenateBlacklist.isOnBlacklist(word)) {
				return word;
			}
			return hyphenator(word);
		}).join(" ");
	}

	$(init);

}
