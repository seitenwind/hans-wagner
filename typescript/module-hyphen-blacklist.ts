module HyphenateBlacklist
{
	let $ = jQuery;

	const BLACKLIST:string[] = [
		"hans",
		"wagner",
		"tradition"
	];

	/**
	 * Checks whether the word is on a blacklist or not
	 *
	 * @param word The word
	 * @returns {boolean} Returns true if the word is on the list
	 */
	export function isOnBlacklist(word:string):boolean
	{
		word = word.trim().toLowerCase();
		return BLACKLIST.indexOf(word) != -1;
	}

}