module VectorLogo
{
	let $ = jQuery;

	function init():void
	{
		$(".av-logo-container img").attr("src", "/wp-content/themes/hanswagner/resources/vectors/logo.svg");
	}

	$(init);
}
