module ImageUtils
{
	let $:JQueryStatic = jQuery;

	export function removeWordpressSize($el:JQuery):void
	{
		let src = $el.attr("src");

		let match = src.match(/-\d+x\d+\./g);
		if(match != null && match.length > 0)
		{
			src = src.replace(match[0], ".");
		}

		$el.attr("src", src);
	}

	function endsWith(str, suffix) {
		return str.indexOf(suffix, str.length - suffix.length) !== -1;
	}
}
