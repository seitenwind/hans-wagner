/*
 * Copyright © 2017 seitenwind
 */


/**
 * Implements anchor navigation functionality
 */
module Anchor
{
    let $: JQueryStatic = jQuery;

    /**
     * Initializes the module
     */
    function init(): void
    {
        let parts = location.href.split("#");
        if(parts.length == 2)
        {
            let anchor = parts[1];
            let offsetTop = $(".anchor-" + anchor).offset().top;
            console.log(offsetTop);
            setTimeout(function ()
            {
                $("body, html").scrollTop(offsetTop + $(window).innerHeight());
            }, 500);
        }
    }

    $(init);
}