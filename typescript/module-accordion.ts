module Accordion
{
	let $:JQueryStatic = jQuery;

	function init(): void
	{
		waitForJQueryUI();
	}

	function waitForJQueryUI()
	{
		if (typeof jQuery.ui !== 'undefined')
		{
			initAccordions();
			initMulticolumnAccordion();
			return;
		}
		requestAnimationFrame(waitForJQueryUI);
	}

	function initAccordions() : void
	{
		let hyphenator = createHyphenator(hyphenationPatternsDe);

		$(".accordion").each(function(index:number, el:Element) {
			let $el = $(el);
			let options = {};

			let active:any = $el.attr("data-option-active");
			if(active == "false") {
				active = false;
			}
			else
			{
				active = parseInt(active);
			}

			options["active"] = active;
			options["collapsible"] = true;
			options["heightStyle"] = "content";

			$el.find("h3.hw-staff-position").each(function(index, h3) {
				$(h3).text(hyphenator($(h3).text()));
			});

			$el.accordion(options);
		});
	}

	function initMulticolumnAccordion(): void
	{
		let colCount = $("[data-staff-contact]").length;
		for(let i = 0; i < colCount; i++) {
			$("[data-staff-contact='" + (i + 1) + "']").on("accordionbeforeactivate", function ()
			{
				for(let j = 0; j < colCount; j++) {
					if(j == i) continue;
					$("[data-staff-contact='" + (j + 1) + "']").accordion("option", "active", false);
				}
			});
		}
	}

	$(init);
}