module Navigation
{
	let $ = jQuery;

	function init(): void
	{
		$(".sub-menu li.current-menu-item").parent().parent().addClass("current-menu-item");
	}

	$(init);
}
