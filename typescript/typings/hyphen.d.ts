interface Hyphenator
{
	(text: string): string;
}

interface HyphenatorOptions
{
	hyphenChar?: string;
	debug?: true;
}

interface HyphenationPattern
{
}

declare var hyphenationPatternsAf: HyphenationPattern;
declare var hyphenationPatternsAs: HyphenationPattern;
declare var hyphenationPatternsBg: HyphenationPattern;
declare var hyphenationPatternsBn: HyphenationPattern;
declare var hyphenationPatternsCa: HyphenationPattern;
declare var hyphenationPatternsCop: HyphenationPattern;
declare var hyphenationPatternsCs: HyphenationPattern;
declare var hyphenationPatternsCu: HyphenationPattern;
declare var hyphenationPatternsCy: HyphenationPattern;
declare var hyphenationPatternsDa: HyphenationPattern;
declare var hyphenationPatternsDe: HyphenationPattern;
declare var hyphenationPatternsDeCh: HyphenationPattern;
declare var hyphenationPatternsElMonoton: HyphenationPattern;
declare var hyphenationPatternsElPolyton: HyphenationPattern;
declare var hyphenationPatternsEnGb: HyphenationPattern;
declare var hyphenationPatternsEnUs: HyphenationPattern;
declare var hyphenationPatternsEo: HyphenationPattern;
declare var hyphenationPatternsEs: HyphenationPattern;
declare var hyphenationPatternsEt: HyphenationPattern;
declare var hyphenationPatternsEu: HyphenationPattern;
declare var hyphenationPatternsFi: HyphenationPattern;
declare var hyphenationPatternsFr: HyphenationPattern;
declare var hyphenationPatternsFur: HyphenationPattern;
declare var hyphenationPatternsGa: HyphenationPattern;
declare var hyphenationPatternsGl: HyphenationPattern;
declare var hyphenationPatternsGrc: HyphenationPattern;
declare var hyphenationPatternsGu: HyphenationPattern;
declare var hyphenationPatternsHi: HyphenationPattern;
declare var hyphenationPatternsHr: HyphenationPattern;
declare var hyphenationPatternsHsb: HyphenationPattern;
declare var hyphenationPatternsHu: HyphenationPattern;
declare var hyphenationPatternsHy: HyphenationPattern;
declare var hyphenationPatternsIa: HyphenationPattern;
declare var hyphenationPatternsId: HyphenationPattern;
declare var hyphenationPatternsIs: HyphenationPattern;
declare var hyphenationPatternsIt: HyphenationPattern;
declare var hyphenationPatternsKa: HyphenationPattern;
declare var hyphenationPatternsKmr: HyphenationPattern;
declare var hyphenationPatternsKn: HyphenationPattern;
declare var hyphenationPatternsLa: HyphenationPattern;
declare var hyphenationPatternsLaClassic: HyphenationPattern;
declare var hyphenationPatternsLaLiturgic: HyphenationPattern;
declare var hyphenationPatternsLt: HyphenationPattern;
declare var hyphenationPatternsLv: HyphenationPattern;
declare var hyphenationPatternsMl: HyphenationPattern;
declare var hyphenationPatternsMn: HyphenationPattern;
declare var hyphenationPatternsMr: HyphenationPattern;
declare var hyphenationPatternsMulEthi: HyphenationPattern;
declare var hyphenationPatternsNb: HyphenationPattern;
declare var hyphenationPatternsNl: HyphenationPattern;
declare var hyphenationPatternsNn: HyphenationPattern;
declare var hyphenationPatternsNo: HyphenationPattern;
declare var hyphenationPatternsOc: HyphenationPattern;
declare var hyphenationPatternsOr: HyphenationPattern;
declare var hyphenationPatternsPa: HyphenationPattern;
declare var hyphenationPatternsPl: HyphenationPattern;
declare var hyphenationPatternsPms: HyphenationPattern;
declare var hyphenationPatternsPt: HyphenationPattern;
declare var hyphenationPatternsRm: HyphenationPattern;
declare var hyphenationPatternsRo: HyphenationPattern;
declare var hyphenationPatternsRu: HyphenationPattern;
declare var hyphenationPatternsSa: HyphenationPattern;
declare var hyphenationPatternsShCyrl: HyphenationPattern;
declare var hyphenationPatternsShLatn: HyphenationPattern;
declare var hyphenationPatternsSk: HyphenationPattern;
declare var hyphenationPatternsSl: HyphenationPattern;
declare var hyphenationPatternsSrCyrl: HyphenationPattern;
declare var hyphenationPatternsSv: HyphenationPattern;
declare var hyphenationPatternsTa: HyphenationPattern;
declare var hyphenationPatternsTe: HyphenationPattern;
declare var hyphenationPatternsTh: HyphenationPattern;
declare var hyphenationPatternsTk: HyphenationPattern;
declare var hyphenationPatternsTr: HyphenationPattern;
declare var hyphenationPatternsUk: HyphenationPattern;
declare var hyphenationPatternsZhLatn: HyphenationPattern;

declare function createHyphenator(pattern:HyphenationPattern, options?:HyphenatorOptions): Hyphenator;
