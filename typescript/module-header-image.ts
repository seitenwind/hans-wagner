module HeaderImage
{
	let $:JQueryStatic = jQuery;

	function init():void
	{
		$(".avia-slide-wrap img").each(function (index: number, el: Element)
		{
			ImageUtils.removeWordpressSize($(el));
		});
	}

	$(init);
}