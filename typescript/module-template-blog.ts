///<reference path="module-ellipsis.ts"/>

module TemplateBlog
{
	import MODE_SENTENCES = Ellipsis.MODE_SENTENCES;
	let $ = jQuery;

	function init(): void
	{
		$(".page .entry-content .read-more-link").each(function (index: number, el: Element)
		{
			let $el = $(el);
			let $parent = $($el.parent().parent());
			$el.detach();
			$parent.append($el);
		});

		$(".blog .post-entry").each(function (index: number, el: Element)
		{
			let $el = $(el);

			let href = $el.find(".post-title a").attr("href");
			let $content = $el.find(".entry-content");

			let $readMoreLink = $("<a></a>");
			$readMoreLink.attr("href", href);
			$readMoreLink.text("Mehr lesen");
			$readMoreLink.addClass("read-more");

			$el.find(".entry-content-wrapper").append($readMoreLink);
		});
	}

	$(init);
}
