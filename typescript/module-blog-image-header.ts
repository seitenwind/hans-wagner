module BlogImageHeader
{

	let $:JQueryStatic = jQuery;

	function init(): void
	{
		$(".big-preview.single-big img").each(function (index:number, el:Element)
		{
			ImageUtils.removeWordpressSize($(el));
		});
	}

	$(init);

}