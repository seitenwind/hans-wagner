<?php
/*
 * Copyright © 2017 seitenwind
 */

/**
 * Provides commonly used setup functions
 */
class Setup
{

    private static $custom_fonts = array();
    private static $shortcode_directories = array();
    private static $font_loading_disabled = false;
    private static $dequeued_styles = array();
    private static $loaded_widgets = array();
    private static $loaded_scripts = array();
    private static $loaded_styles = array();

    /** prevent instantiation */
    private function __construct()
    {
    }

    /**
     * Disables automatic linebreaks by removing the wpautop filter.
     */
    public static function disable_automatic_linebreaks()
    {
        remove_filter('the_content', 'wpautop');
    }

    /**
     * Enables php error reporting
     */
    public static function enable_error_reporting()
    {
        ini_set('display_errors', 1);
        error_reporting(E_ALL ^ E_STRICT);
    }

    /**
     * Enables frontend login authorization
     */
    public static function enable_frontend_login_authorization()
    {
        add_action('template_redirect', function () {
            if (!is_user_logged_in() && !is_feed()) auth_redirect();
        });
    }

    /**
     * Dequeues an enfold base style. Use Enfold::STYLE_XXX to select your preferred style
     */
    public static function dequeue_enfold_style($style_name, $frontend_only = true)
    {
        array_push(self::$dequeued_styles, array(
            "name" => $style_name,
            "frontend_only" => $frontend_only
        ));
    }

    /**
     * Enable avia template builder custom css
     */
    public static function avia_enable_template_builder_custom_css()
    {
        add_theme_support('avia_template_builder_custom_css');
    }

    /**
     * Registers a script for loading
     * @param $identifier string the identifier for this script
     * @param $path string The relative path to the script
     * @param array $dependencies The dependencies for the script
     */
    public static function load_script($identifier, $path = null, $dependencies = array())
    {
        array_push(self::$loaded_scripts, array(
            "identifier" => $identifier,
            "path" =>  $path,
            "dependencies" => $dependencies
        ));
    }

    /**
     * Registers a script for loading
     * @param $identifier string the identifier for this script
     * @param $path string The relative path to the script
     * @param array $dependencies The dependencies for the script
     */
    public static function load_style($identifier, $path = null, $dependencies = array())
    {
        array_push(self::$loaded_styles, array(
            "identifier" => $identifier,
            "path" =>  $path,
            "dependencies" => $dependencies
        ));
    }

    /**
     * Adds a custom font to the avia font-selector. Provide a description in the following format:
     *
     * font_name: \w+
     * font_weights: (\ni?,?)+
     *
     * font_name:font_weights
     *
     * ex.: "Oswald:200,300,400,500,600,700"
     *
     * @param $fontDescription string font description
     */
    public static function add_custom_font($fontName, $fontDescription)
    {
        array_push(self::$custom_fonts, array(
            "name" => $fontName,
            "description" => $fontDescription
        ));
    }

    /**
     * Disables font loading from google.
     */
    public static function disable_loading_google_fonts()
    {
        if (self::$font_loading_disabled) return;
        add_action('init', function () {
            global $avia;
            $avia->style->print_extra_output = false;
        });
        self::$font_loading_disabled = true;
    }

    /**
     * Disables various feeds
     */
    public static function disable_feeds()
    {
        function disable_feed()
        {
            // do nothing
        }

        add_action('do_feed', 'disableFeed', 1);
        add_action('do_feed_rdf', 'disableFeed', 1);
        add_action('do_feed_rss', 'disableFeed', 1);
        add_action('do_feed_rss2', 'disableFeed', 1);
        add_action('do_feed_atom', 'disableFeed', 1);
    }

    /**
     * Registers the specified directory to load shortcodes from.
     *
     * If no directory is specified, the default "[child]/php/shortcodes" will be used.
     *
     * @param string $directory The directory
     */
    public static function register_shortcode_directory($directory = null)
    {
        if ($directory == null) {
            $directory = get_stylesheet_directory() . "/php/shortcodes/";
        }

        if (substr($directory, strlen($directory) - 1, 1) != "/") {
            $directory .= "/";
        }

        array_push(self::$shortcode_directories, $directory);
    }

    /**
     * Registers a widget for loading.
     *
     * @param $widget_class_name string The widget class name as string.
     */
    public static function load_widget($widget_class_name)
    {
        array_push(self::$loaded_widgets, $widget_class_name);
    }

    /**
     * Applies the setup
     */
    public static function do_setup()
    {
        self::load_fonts();
        self::load_shortcode_directories();
        self::enqueue_scripts();
        self::enqueue_styles();
        self::enqueue_backend_styles();
        self::dequeue_enfold_styles();
        self::load_widgets();
    }

    /**
     * Loads all registered widgets
     */
    private static function load_widgets()
    {
        add_action('widgets_init', function(){
            foreach (self::$loaded_widgets as $key => $widget)
            {
                register_widget($widget);
            }
        });
    }

    /**
     * Loads shortcode directories
     */
    private static function load_shortcode_directories()
    {
        // source: http://www.kriesi.at/support/topic/avia-template-builder-avia-shortcodes-add-custom-shortcode/
        add_filter('avia_load_shortcodes', function ($paths) {
            foreach (self::$shortcode_directories as $key => $shortcode_directory) {
                array_unshift($paths, $shortcode_directory);
            }
            return $paths;
        }, 15, 1);
    }

    /**
     * Loads custom fonts
     */
    private static function load_fonts()
    {
        $setupAviaFonts = function ($fonts) {
            foreach (self::$custom_fonts as $key => $font) {
                $fonts[$font['name']] = $font['description'];
            }
            return $fonts;
        };

        add_filter('avf_google_content_font', $setupAviaFonts);
        add_filter('avf_google_heading_font', $setupAviaFonts);
    }

    /**
 * Enqueues the default scripts for the template
 */
    private static function enqueue_scripts()
    {
        add_action('wp_enqueue_scripts', function () {
            foreach (self::$loaded_scripts as $key => $script) {
                wp_enqueue_script($script["identifier"], get_stylesheet_directory_uri() . $script["path"], $script["dependencies"]);
            }
        });
    }

    /**
     * Enqueues the default scripts for the template
     */
    private static function enqueue_styles()
    {
        add_action('wp_enqueue_scripts', function () {
            foreach (self::$loaded_styles as $key => $style) {
                wp_enqueue_style($style["identifier"], get_stylesheet_directory_uri() . $style["path"], $style["dependencies"]);
            }
        });
    }

    /**
     * Enqueues additional styles for the backend
     */
    private static function enqueue_backend_styles()
    {
        add_action('admin_head', function () {
            $href = get_stylesheet_directory_uri() . "/css/admin.min.css";
            echo "<link rel='stylesheet' href='$href' type='text/css' media='all'>";
        });
    }

    /**
     * Dequeues all styles
     */
    private static function dequeue_enfold_styles()
    {
        add_action('wp_enqueue_scripts', function () {
            foreach (self::$dequeued_styles as $key => $style) {

                if(is_admin()) {
                    if(!$style["frontent_only"]) {
                        wp_dequeue_style($style["name"]);
                    }
                }
                else {
                    wp_dequeue_style($style["name"]);
                }
            }
        }, 100);
    }


}