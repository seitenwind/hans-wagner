<?php
/**
 * Copyright © 2017 seitenwind
 */

/**
 * Deals shortcode
 *
 * Displays a current deals
 */

if ( ! class_exists( 'Deals_Shortcode' ) ) {

	class Deals_Shortcode extends aviaShortcodeTemplate {

		/**
		 * Create the config array for the shortcode button
		 */
		function shortcode_insert_button() {
			$this->config['name']             = __( 'Aktuelle Angebote', 'avia_framework' );
			$this->config['tab']              = __( 'Content Elements', 'avia_framework' );
			$this->config['icon']             = get_stylesheet_directory_uri() . "/resources/images/icon-deal.png";
			$this->config['order']            = 130;
			$this->config['shortcode']        = 'hw-deals';
			$this->config['shortcode_nested'] = array( 'hw-deal' );
			$this->config['tooltip']          = __( 'Zeigt aktuelle Angebote an', 'avia_framework' );
			$this->config['target']           = 'avia-target-insert';
			$this->config['tinyMCE']          = array( 'tiny_always' => true );
		}

		/**
		 * Creates the select key-value array for the avia editor using the deals model
		 *
		 * @return mixed
		 */
		private function create_deals_select_values() {
			$dealsModel = Deals_Model::get_instance();

			$result = [];
			foreach ( $dealsModel->fetch_all_deals() as $deal ) {
				$result[ $deal->get_title() ] = $deal->get_post_id();
			}

			return $result;
		}

		/**
		 * Popup Elements
		 *
		 * If this function is defined in a child class the element automatically gets an edit button, that, when pressed
		 * opens a modal window that allows to edit the element properties
		 *
		 * @return void
		 */
		function popup_elements() {
			$this->elements = [
				[
					"type"          => "tab_container",
					'nodescription' => true
				],
				[
					"type"          => "tab",
					"name"          => __( "Content", 'avia_framework' ),
					'nodescription' => true
				],
				[
					"type"            => "modal_group",
					"id"              => "content",
					'container_class' => "avia-element-fullwidth avia-multi-img",
					"modal_title"     => __( "Select Deal", 'hans_wagner' ),
					"add_label"       => __( "Add deal", 'hans_wagner' ),
					"std"             => [],

					'subelements' => [
						[
							"name"    => __( "Select a deal", 'hans_wagner' ),
							"desc"    => __( "Choose a deal to display.", 'hans_wagner' ),
							"id"      => "post_id",
							"type"    => "select",
							"std"     => "",
							"subtype" => $this->create_deals_select_values()
						]
					],
				],
				[
					"type"          => "close_div",
					'nodescription' => true
				],
				[
					"type"          => "close_div",
					'nodescription' => true
				],
			];
		}

		/**
		 * Editor Element - this function defines the visual appearance of an element on the AviaBuilder Canvas
		 * Most common usage is to define some markup in the $params['innerHtml'] which is then inserted into the drag and drop container
		 * Less often used: $params['data'] to add data attributes, $params['class'] to modify the className
		 *
		 *
		 * @param array $params this array holds the default values for $content and $args.
		 *
		 * @return array $params the return array usually holds an innerHtml key that holds item specific markup.
		 */
		function editor_element( $params ) {
			extract( av_backend_icon( $params ) ); // creates $font and $display_char if the icon was passed as param "icon" and the font as "font"
			extract( shortcode_atts( array(
				'count'        => '',
				'custom_class' => '',
			), $params['args'], $this->config['shortcode'] ) );


			$inner = "<div class='avia_icon_element avia_textblock avia_textblock_style'>";
			$inner .= "		<div class='hw-deals-content-wrap'>";
			$inner .= "         <img src='" . get_stylesheet_directory_uri() . "/resources/images/icon-deal.png'>";
			$inner .= "     </div>";
			$inner .= "</div>";

			$params['innerHtml'] = $inner;
			$params['class']     = "";

			return $params;
		}

		/**
		 * Editor Sub Element - this function defines the visual appearance of an element that is displayed within a modal window and on click opens its own modal window
		 * Works in the same way as Editor Element
		 *
		 * @param array $params this array holds the default values for $content and $args.
		 *
		 * @return $params the return array usually holds an innerHtml key that holds item specific markup.
		 */
		function editor_sub_element( $params ) {
			$dealsModel = Deals_Model::get_instance();

			$img_template   = $this->update_template( "img_fakeArg", "{{img_fakeArg}}" );
			$title_template = $this->update_template( "title", "{{title}}" );

			$deal = $dealsModel->fetch_deal_by_id( $params['args']['post_id'] );

			$thumbnail_id = $deal->get_image_id();
			$thumbnail    = isset( $thumbnail_id ) ? wp_get_attachment_image( $thumbnail_id ) : "";

			$params['innerHtml'] = "";
			$params['innerHtml'] .= "<div class='avia_title_container'>";
			$params['innerHtml'] .= "	<div class='avia-deal-subelement'>";
			$params['innerHtml'] .= "		<span class='avia_slideshow_image' {$img_template} >{$thumbnail}</span>";
			$params['innerHtml'] .= "		<div class='avia_slideshow_content'>";
			$params['innerHtml'] .= "			<h4 class='avia_title_container_inner' {$title_template} >" . $deal->get_title() . "</h4>";
			$params['innerHtml'] .= "		</div>";
			$params['innerHtml'] .= "	</div>";
			$params['innerHtml'] .= "</div>";

			return $params;
		}

		/**
		 * Frontend Shortcode Handler
		 *
		 * @param array $atts array of attributes
		 * @param string $content text within enclosing form of shortcode element
		 * @param string $shortcodename the shortcode found, when == callback name
		 *
		 * @return string $output returns the modified html string
		 */
		function shortcode_handler( $atts, $content = "", $shortcodename = "", $meta = "" ) {
			extract( shortcode_atts( [
				'content' => ShortcodeHelper::shortcode2array( $content, 1 ),
			], $atts, $this->config['shortcode'] ) );

			$deals_model = Deals_Model::get_instance();

			$result = "";

			foreach ($content as $index => $deal_shortcode)
			{
				$deal = $deals_model->fetch_deal_by_id($deal_shortcode["attr"]["post_id"]);
				$result .= $this->render_column($this->render_deal($deal), $index);
			}

			return $result;
		}

		/**
		 * Renders a deal
		 *
		 * @param Deal_Model $deal
		 *
		 * @return string
		 */
		private function render_deal( $deal ) {
			$result = "";

			$image_uri = $deal->get_image_uri();
			$pdf_uri   = $deal->get_pdf_uri();
			$title     = $deal->get_title();

			$result .= "<div class='hw-deal'>";
			$result .= "    <div class='hw-deal-image'>";
			$result .= "        <img src='$image_uri'>";
			$result .= "    </div>";
			$result .= "    <h3 class='hw-deal-headline'>$title</h3>";
			$result .= $this->render_button( $pdf_uri );
			$result .= "</div>";

			return $result;
		}

		/**
		 * Renders a button
		 *
		 * @param string $id
		 * @param string $class
		 * @param string $alignment
		 * @param string $size
		 *
		 * @return string
		 */
		private function render_button( $id, $class = "hw-deal-button", $alignment = "center", $size = "small" ) {
			$result = "";

			$href = wp_get_attachment_url( $id );

			$result .= "<div class='avia-button-wrap avia-button-$alignment'>";
			$result .= "    <a target='_blank' class='avia-button avia-icon_select-no avia-size-$size avia-position-$alignment' href='$href'>";
			$result .= "        <span class='$class' data-av_icon='' data-av_iconfont='entypo-fontello'></span> ";
			$result .= "        Herunterladen";
			$result .= "    </a>";
			$result .= "</div>";

			return $result;
		}

		/**
		 * Renders a column
		 *
		 * @param string $content Add content into this column
		 * @param $column_index int index of the rendered column
		 *
		 * @return string
		 */
		private function render_column( $content, $column_index ) {
			$column_class = "av_one_third flex_column";

			if ( $column_index % 3 == 0 ) {
				$column_class .= " first";
			}

			$render = "<div class='$column_class'>";
			if ( $content != null ) {
				$render .= $content;
			}
			$render .= "</div>";

			return $render;
		}

	}
}
