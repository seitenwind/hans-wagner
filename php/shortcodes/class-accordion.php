<?php
/**
 * Copyright © 2017 seitenwind
 */

/**
 * Accordion shortcode
 *
 * Overrides the default accordion toggle from enfold.
 */
if (!class_exists('Accordion_Shortcode')) {

    class avia_sc_toggle {} // create class to disable default accordion

    class Accordion_Shortcode extends aviaShortcodeTemplate
    {
        static $toggle_id = 1;
        static $counter = 1;
        static $initial = 0;
        static $tags = array();

        /**
         * Create the config array for the shortcode button
         */
        function shortcode_insert_button()
        {
            $this->config['name'] = __('Accordion', 'avia_framework');
            $this->config['tab'] = __('Content Elements', 'avia_framework');
            $this->config['icon'] = AviaBuilder::$path['imagesURL'] . "sc-accordion.png";
            $this->config['order'] = 70;
            $this->config['target'] = 'avia-target-insert';
            $this->config['shortcode'] = 'av_toggle_container';
            $this->config['shortcode_nested'] = array('accordion_panel');
            $this->config['tooltip'] = __('Creates toggles or accordions', 'avia_framework');
        }


        function extra_assets()
        {
            if (is_admin()) {
                $ver = AviaBuilder::VERSION;
                wp_enqueue_script('avia_tab_toggle_js', AviaBuilder::$path['assetsURL'] . 'js/avia-tab-toggle.js', array('avia_modal_js'), $ver, TRUE);
            }
        }

        /**
         * Popup Elements
         *
         * If this function is defined in a child class the element automatically gets an edit button, that, when pressed
         * opens a modal window that allows to edit the element properties
         *
         * @return void
         */
        function popup_elements()
        {
            $this->elements = array(


                array(
                    "name" => __("Add/Edit Toggles", 'avia_framework'),
                    "desc" => __("Here you can add, remove and edit the toggles you want to display.", 'avia_framework'),
                    "type" => "modal_group",
                    "id" => "content",
                    "modal_title" => __("Edit Form Element", 'avia_framework'),
                    "std" => array(

                        array('title' => __('Toggle 1', 'avia_framework'), 'tags' => ''),
                        array('title' => __('Toggle 2', 'avia_framework'), 'tags' => ''),

                    ),

                    'subelements' => array(

                        array(
                            "name" => __("Toggle Title", 'avia_framework'),
                            "desc" => __("Enter the toggle title here (Better keep it short)", 'avia_framework'),
                            "id" => "title",
                            "std" => "Toggle Title",
                            "type" => "input"),

                        array(
                            "name" => __("Toggle Bild", 'avia_framework'),
                            "desc" => __("Zeigt ein Bild im Toggle-Header an.", 'avia_framework'),
                            "id" 	=> "image",
                            "type" 	=> "image",
                            "title" => __("Insert Image",'avia_framework' ),
                            "button" => __("Insert",'avia_framework' ),
                            "std" 	=> AviaBuilder::$path['imagesURL']."placeholder.jpg"),

                        array(
                            "name" => __("Toggle Content", 'avia_framework'),
                            "desc" => __("Enter some content here", 'avia_framework'),
                            "id" => "content",
                            "type" => "tiny_mce",
                            "std" => __("Toggle Content goes here", 'avia_framework'),
                        ),

                        array(
                            "name" => __("Toggle Sorting Tags", 'avia_framework'),
                            "desc" => __("Enter any number of comma separated tags here. If sorting is active the user can filter the visible toggles with the help of these tags", 'avia_framework'),
                            "id" => "tags",
                            "std" => "",
                            "type" => "input"),

                    )
                ),

                array(
                    "name" => __("Initial Open", 'avia_framework'),
                    "desc" => __("Enter the Number of the Accordion Item that should be open initially. Set to Zero if all should be close on page load ", 'avia_framework'),
                    "id" => "initial",
                    "std" => "0",
                    "type" => "input"),


                array(
                    "name" => __("Behavior", 'avia_framework'),
                    "desc" => __("Should only one toggle be active at a time and the others be hidden or can multiple toggles be open at the same time?", 'avia_framework'),
                    "id" => "mode",
                    "type" => "select",
                    "std" => "accordion",
                    "subtype" => array(__('Only one toggle open at a time (Accordion Mode)', 'avia_framework') => 'accordion', __("Multiple toggles open allowed (Toggle Mode)", 'avia_framework') => 'toggle')
                ),

                array(
                    "name" => __("Sorting", 'avia_framework'),
                    "desc" => __("Display the toggle sorting menu? (You also need to add a number of tags to each toggle to make sorting possible)", 'avia_framework'),
                    "id" => "sort",
                    "type" => "select",
                    "std" => "",
                    "subtype" => array(__('No Sorting', 'avia_framework') => '', __("Sorting Active", 'avia_framework') => 'true')
                ),

            );


            if (current_theme_supports('avia_template_builder_custom_tab_toogle_id')) {
                $this->elements[0]['subelements'][] = array(
                    "name" => __("For Developers: Custom Toggle ID", 'avia_framework'),
                    "desc" => __("Insert a custom ID for the element here. Make sure to only use allowed characters", 'avia_framework'),
                    "id" => "custom_id",
                    "type" => "input",
                    "std" => "");
            }


        }

        /**
         * Editor Sub Element - this function defines the visual appearance of an element that is displayed within a modal window and on click opens its own modal window
         * Works in the same way as Editor Element
         * @param array $params this array holds the default values for $content and $args.
         * @return $params the return array usually holds an innerHtml key that holds item specific markup.
         */
        function editor_sub_element($params)
        {
            $template = $this->update_template("title", "{{title}}");

            $params['innerHtml'] = "";
            $params['innerHtml'] .= "<div class='avia_title_container' {$template}>" . $params['args']['title'] . "</div>";

            return $params;
        }

        /**
         * Renders the options array
         *
         * @param array $options
         * @return string
         */
        function render_options($options)
        {
            $result = array();
            foreach($options as $key => $value)
            {
                array_push($result, "data-option-$key='$value'");
            }
            return join(" ", $result);
        }

        /**
         * Frontend Shortcode Handler
         *
         * @param array $atts array of attributes
         * @param string $content text within enclosing form of shortcode element
         * @param string $shortcodename the shortcode found, when == callback name
         * @return string $output returns the modified html string
         */
        function shortcode_handler($atts, $content = "", $shortcodename = "", $meta = "")
        {
            $atts = shortcode_atts(array(
                'initial' => '0',
                'mode' => 'accordion',
                'sort' => ''),
            $atts, $this->config['shortcode']);
            extract($atts);

            $options = array();

            /** @noinspection PhpUndefinedVariableInspection */
            if ($mode == 'accordion') {
                $options["collapsible"] = "true";
            }

            /** @noinspection PhpUndefinedVariableInspection */
            if($initial != null && $initial > 0) {
                $options["active"] = $initial - 1;
            }

            $options["header"] = ".accordion-header";

            $options = $this->render_options($options);

            $output = '<div class="accordion ' . $meta['el_class'] . '" ' . $options . '>';
            $output .= ShortcodeHelper::avia_remove_autop($content, true);
            $output .= '</div>';

            return $output;
        }


        /**
         * Renders the single accordion element
         *
         * @param $atts
         * @param string $content
         * @param string $shortcodename
         * @return string
         */
        function accordion_panel($atts, $content = "", $shortcodename = "")
        {
            $toggle_atts = shortcode_atts(array(
                'title' => '',
                'tags' => '',
                'image' => '',
                'src' => '',
                'custom_id' => '',
                'custom_markup' => ''),
            $atts, 'accordion_panel');

            if (empty($toggle_atts['custom_id'])) {
                $toggle_atts['custom_id'] = 'toggle-' . Accordion_Shortcode::$toggle_id++;
            }

            $image = $atts["image"];
            $title = $atts["title"];

            $id = $toggle_atts['custom_id'];

            $output  = "<h3 id='$id'><img src='$image'> <span class='accordion-title'>$title</span></h3>";
            $output .= "<div>";
            $output .= ShortcodeHelper::avia_apply_autop(ShortcodeHelper::avia_remove_autop($content));
            $output .= "</div>";

            return $output;
        }

        /**
         * Fetches the image source
         *
         * @param $src
         * @return string
         */
        private function load_image($src)
        {
            $template = $this->update_template("src", "<img src='{{src}}' alt=''/>");
            $img	  = "";

            if(!empty($params['args']['attachment']) && !empty($params['args']['attachment_size']))
            {
                $img = wp_get_attachment_image($params['args']['attachment'],$params['args']['attachment_size']);
            }
            else if(isset($params['args']['src']) && is_numeric($params['args']['src']))
            {
                $img = wp_get_attachment_image($params['args']['src'],'large');
            }
            else if(!empty($params['args']['src']))
            {
                $img = "<img src='".$params['args']['src']."' alt=''  />";
            }

            return $template;
        }

    }
}
