<?php
/**
 * Copyright © 2017 seitenwind
 */

/**
 * Download shortcode
 *
 * Displays a download item
 */

if (!class_exists('SpecialHeadline_Shortcode')) {

    class SpecialHeadline_Shortcode extends aviaShortcodeTemplate
    {
        /**
         * Create the config array for the shortcode button
         */
        function shortcode_insert_button()
        {
            $this->config['name'] = __('Spezial Überschrift', 'avia_framework');
            $this->config['tab'] = __('Content Elements', 'avia_framework');
            $this->config['icon'] = get_stylesheet_directory_uri() . "/resources/images/icon-special-headline.png";
            $this->config['order'] = 130;
            $this->config['shortcode'] = 'hw-special-headline';
            $this->config['tooltip'] = __('Zeigt eine spezielle Überschrift an', 'avia_framework');
            $this->config['target'] = 'avia-target-insert';
            $this->config['tinyMCE'] = array('tiny_always' => true);
        }

        /**
         * Popup Elements
         *
         * If this function is defined in a child class the element automatically gets an edit button, that, when pressed
         * opens a modal window that allows to edit the element properties
         *
         * @return void
         */
        function popup_elements()
        {
            $this->elements = array(

                array(
                    "type" => "tab_container", 'nodescription' => true
                ),

                array(
                    "type" => "tab",
                    "name" => __("Content", 'avia_framework'),
                    'nodescription' => true
                ),

                array(
                    "name" => __("Titel", 'avia_framework'),
                    "desc" => __("A small caption below the icon", 'avia_framework'),
                    "id" => "title",
                    "type" => "input",
                    "std" => ""
                ),

                array(
                    "name" => __("Untertitel", 'avia_framework'),
                    "desc" => __("A small caption below the icon", 'avia_framework'),
                    "id" => "subtitle",
                    "type" => "input",
                    "std" => ""
                ),

                array(
                    "name" => __("Tag", 'avia_framework'),
                    "desc" => __("Überschrift Tag", 'avia_framework'),
                    "id" => "tag",
                    "type" => "select",
                    "std" => "h1",
                    "subtype" => array(
                        __('H1', 'avia_framework') => 'h1',
                        __('H2', 'avia_framework') => 'h2',
                        __('H3', 'avia_framework') => 'h3',
                        __('H4', 'avia_framework') => 'h4',
                        __('H5', 'avia_framework') => 'h5',
                        __('H6', 'avia_framework') => 'h6',
                    )
                ),

                array(
                    "type" => "close_div",
                    'nodescription' => true
                ),

                array(
                    "type" => "close_div",
                    'nodescription' => true
                ),

            );
        }

        /**
         * Editor Element - this function defines the visual appearance of an element on the AviaBuilder Canvas
         * Most common usage is to define some markup in the $params['innerHtml'] which is then inserted into the drag and drop container
         * Less often used: $params['data'] to add data attributes, $params['class'] to modify the className
         *
         *
         * @param array $params this array holds the default values for $content and $args.
         * @return array $params the return array usually holds an innerHtml key that holds item specific markup.
         */
        function editor_element($params)
        {
            extract(av_backend_icon($params)); // creates $font and $display_char if the icon was passed as param "icon" and the font as "font"
            extract(shortcode_atts(array(
                'title' => '',
                'subtitle' => '',
                'tag' => '',
                'custom_class' => '',
            ), $params['args'], $this->config['shortcode']));


            $inner = "<div class='avia_icon_element avia_textblock avia_textblock_style'>";
            $inner .= "		<div class='hw-special-headline-content-wrap'>";

            /** @noinspection PhpUndefinedVariableInspection */
            $inner .= "			<$tag class='hw-special-headline-title' data-update_with='title'>" . $title . "</$tag>";

            /** @noinspection PhpUndefinedVariableInspection */
            $inner .= "			<$tag class='hw-special-headline-subtitle' data-update_with='subtitle'>" . $subtitle . "</$tag>";

            $inner .= "		</div>";
            $inner .= "</div>";

            $params['innerHtml'] = $inner;
            $params['class'] = "";

            return $params;
        }


        /**
         * Frontend Shortcode Handler
         *
         * @param array $atts array of attributes
         * @param string $content text within enclosing form of shortcode element
         * @param string $shortcodename the shortcode found, when == callback name
         * @return string $output returns the modified html string
         */
        function shortcode_handler($atts, $content = "", $shortcodename = "", $meta = "")
        {
            //this is a fix that solves the false paragraph removal by wordpress if the dropcaps shortcode is used at the beginning of the content of single posts/pages
            global $post, $avia_add_p;

            $add_p = "";
            $custom_class = !empty($meta['custom_class']) ? $meta['custom_class'] : "";
            if (isset($post->post_content) && strpos($post->post_content, '[hw-special-headline') === 0 && $avia_add_p == false && is_singular()) {
                $add_p = "<p>";
                $avia_add_p = true;
            }

            extract(shortcode_atts(array(
                'title' => '',
                'subtitle' => '',
                'tag' => '',
                'custom_class' => '',
            ), $atts, $this->config['shortcode']));

            $result = "<{$tag} class='hw-special-headline'>";
            $result .= "<span class='first-line'>{$title}</span><br/>";
            if($subtitle != null && strlen($subtitle) > 0) {
                $result .= "<span class='second-line'>{$subtitle}</span>";
            }
            $result .= "</{$tag}>";

            return $result;
        }

    }
}
