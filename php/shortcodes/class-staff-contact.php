<?php
/**
 * Copyright © 2017 seitenwind
 */

/**
 * Download shortcode
 *
 * Displays a download item
 */
if (!class_exists('StaffContact_Shortcode')) {

    class StaffContact_Shortcode extends aviaShortcodeTemplate
    {

        /**
         * @var int
         */
        private static $current_staff_element = 1;

        /**
         * @var string
         */
        private static $column_class = array();

        /**
         * @var array
         */
        private static $staff_members = array();

        /**
         * Create the config array for the shortcode button
         */
        function shortcode_insert_button()
        {
            $this->config['name'] = __('Mitarbeiter Liste', 'avia_framework');
            $this->config['tab'] = __('Content Elements', 'avia_framework');
            $this->config['icon'] = get_stylesheet_directory_uri() . "/resources/images/icon-staff-contact.png";
            $this->config['order'] = 133;
            $this->config['shortcode'] = 'hw-staff-contact';
            $this->config['shortcode_nested'] = array('hw_staff_member_single');
            $this->config['tooltip'] = __('Zeigt eine Liste mit Mitarbeitern und Kontaktdaten an', 'avia_framework');
            $this->config['target'] = 'avia-target-insert';
            $this->config['tinyMCE'] = array('tiny_always' => true);
        }

        /**
         * Popup Elements
         *
         * If this function is defined in a child class the element automatically gets an edit button, that, when pressed
         * opens a modal window that allows to edit the element properties
         *
         * @return void
         */
        function popup_elements()
        {
            $this->elements = array(

                array(
                    "type" => "tab_container", 'nodescription' => true
                ),

                array(
                    "type" => "tab",
                    "name" => __("Content", 'avia_framework'),
                    'nodescription' => true
                ),

                array(
                    "name" => __("Mitarbeiter hinzufügen/bearbeiten", 'avia_framework'),
                    "desc" => __("Hier können Sie Mitarbeiter hinzufügen und oder bearbeiten.", 'avia_framework'),
                    "type" => "modal_group",
                    "id" => "content",
                    "modal_title" => __("Mitarbeiter bearbeiten", 'avia_framework'),
                    "std" => array(
                        array('name' => __('Name', 'avia_framework'), 'Subtitle' => '', 'check' => 'is_empty'),
                    ),

                    'subelements' => array(

                        array(
                            "name" => __("Position", 'avia_framework'),
                            "desc" => __("Geben Sie die Position des Mitarbeiters ein.", 'avia_framework'),
                            "id" => "position",
                            "std" => "",
                            "type" => "input"),

                        array(
                            "name" => __("Name", 'avia_framework'),
                            "desc" => __("Geben Sie den Namen des Mitarbeiters ein.", 'avia_framework'),
                            "id" => "name",
                            "std" => "",
                            "type" => "input"),

                        array(
                            "name" => __("Telefonnummer", 'avia_framework'),
                            "desc" => __("Geben Sie die Telefonnummer des Mitarbeiters ein.", 'avia_framework'),
                            "id" => "phone",
                            "std" => "",
                            "type" => "input"
                        ),

                        array(
                            "name" => __("E-Mail Adresse", 'avia_framework'),
                            "desc" => __("Geben Sie die E-Mail Adresse des Mitarbeiters ein.", 'avia_framework'),
                            "id" => "email",
                            "std" => "",
                            "type" => "input"),

                        array(
                            "name" => __("Alternative Telefonnummer", 'avia_framework'),
                            "desc" => __("Geben Sie eine alternative Telefonnummer an (optional).", 'avia_framework'),
                            "id" => "phone_alternate",
                            "std" => "",
                            "type" => "input"),
                    )
                ),

                array(
                    "name" => __("Testimonial Grid Columns", 'avia_framework'),
                    "desc" => __("How many columns do you want to display", 'avia_framework'),
                    "id" => "columns",
                    "required" => array('style', 'equals', 'grid'),
                    "type" => "select",
                    "std" => "2",
                    "subtype" => AviaHtmlHelper::number_array(1, 4, 1)
                ),

                array(
                    "type" => "close_div",
                    'nodescription' => true
                ),

                array(
                    "type" => "close_div",
                    'nodescription' => true
                ),

            );
        }

        /**
         * Editor Element - this function defines the visual appearance of an element on the AviaBuilder Canvas
         * Most common usage is to define some markup in the $params['innerHtml'] which is then inserted into the drag and drop container
         * Less often used: $params['data'] to add data attributes, $params['class'] to modify the className
         *
         *
         * @param array $params this array holds the default values for $content and $args.
         * @return array $params the return array usually holds an innerHtml key that holds item specific markup.
         */
        function editor_element($params)
        {
            extract(av_backend_icon($params)); // creates $font and $display_char if the icon was passed as param "icon" and the font as "font"
            extract(shortcode_atts(array(
                'headline' => '',
                'subtitle' => '',
                'tag' => '',
                'src' => '',
                'buttonlabel' => '',
                'custom_class' => '',
            ), $params['args'], $this->config['shortcode']));


            $icon_src = $this->config['icon'];

            $inner = "<div class='avia_icon_element avia_textblock avia_textblock_style'>";
            $inner .= "    <div class='hw-staff-contact-content-wrap'>";
            $inner .= "        <img src='$icon_src'>";
            $inner .= "    </div>";
            $inner .= "</div>";

            $params['innerHtml'] = $inner;
            $params['class'] = "";

            return $params;
        }


        /**
         * Editor Sub Element - this function defines the visual appearance of an element that is displayed within a modal window and on click opens its own modal window
         * Works in the same way as Editor Element
         * @param array $params this array holds the default values for $content and $args.
         * @return array $params the return array usually holds an innerHtml key that holds item specific markup.
         */
        function editor_sub_element($params)
        {
            $template = $this->update_template("name", __("Mitarbeiter", 'avia_framework') . ": {{name}}");

            $params['innerHtml'] = "";
            $params['innerHtml'] .= "<div class='avia_title_container' {$template}>" . __("Mitarbeiter", 'avia_framework') . ": " . $params['args']['name'] . "</div>";

            return $params;
        }

        /**
         * Frontend Shortcode Handler
         *
         * @param array $atts array of attributes
         * @param string $content text within enclosing form of shortcode element
         * @param string $shortcodename the shortcode found, when == callback name
         * @param string $meta
         * @return string $output returns the modified html string
         */
        function shortcode_handler($atts, $content = "", $shortcodename = "", $meta = "")
        {
            $atts = shortcode_atts(array(

                'style' => "grid",
                'columns' => "2",
                "autoplay" => true,
                "interval" => 5,
                'font_color' => '',
                'custom_title' => '',
                'custom_content' => '',
            ), $atts, $this->config['shortcode']);

            extract($atts);

            $column_class = "av_one_full flex_column no_margin";

            /** @noinspection PhpUndefinedVariableInspection */
            switch ($columns) {
                case 1:
                    $column_class = "av_one_full flex_column";
                    break;
                case 2:
                    $column_class = "av_one_half flex_column";
                    break;
                case 3:
                    $column_class = "av_one_third flex_column";
                    break;
                case 4:
                    $column_class = "av_one_fourth flex_column";
                    break;
            }

            $render = "";

            self::$staff_members[self::$current_staff_element] = array();
            self::$column_class[self::$current_staff_element] = $column_class;

            // parse nested shortcodes
            ShortcodeHelper::avia_remove_autop($content, true);

            $column_content = array();

            $current_column = 0;

            $position_order = $this->calculate_position_order(self::$staff_members[self::$current_staff_element]);

            foreach ($position_order as $index => $position) {
                $staff = self::$staff_members[self::$current_staff_element][$position];
                $rendered_members = $this->render_staff_members($position, $staff);

                if ($column_content[$current_column] == null) {
                    $column_content[$current_column] = "";
                }
                $column_content[$current_column] .= $rendered_members;

                $current_column = ($current_column + 1) % $columns;
            }

            for ($i = 0; $i < $columns; $i++) {
                $content = $column_content[$i];
                $active = $i == 0 ? 0 : false;
                $number = $i + 1;
                $content = "<div class='accordion staff-contact-accordion' data-staff-contact='$number' data-option-active='$active'>$content</div>";
                $render .= $this->render_column($content, $i);
            }

            self::$current_staff_element++;

            return $render;
        }

        /**
         * Converts a local phone number to an international number
         *
         * TODO: create element to specify phone country
         *
         * @param string $phone_number The phone number
         * @param string $country_code The country code of the phone number
         * @return mixed|string
         */
        private function convert_phone_format($phone_number, $country_code = "+49")  // assume all phone numbers are german numbers
        {
            $phone_number = str_replace(" ", "", $phone_number);
            $phone_number = str_replace("-", "", $phone_number);
            $phone_number = substr($phone_number, 1);
            $phone_number = $country_code . $phone_number;

            return $phone_number;
        }

        /**
         * Renders a group of staff members, grouped by their position.
         *
         * @param string $position
         * @param array $members
         * @return string
         */
        private function render_staff_members($position, $members)
        {
            $render = "";

            $render .= "<h3 class='hw-staff-position'>$position</h3>";
            $render .= "<div class='hw-staff-members-container'>";

            foreach ($members as $index => $member) {
                $render .= "<div class='hw-staff-member-container'>";

                $phone_international = $this->convert_phone_format($member['phone']);
                $email = $member['email'];

                $render .= "<h4 class='hw-staff-name'>" . $member["name"] . "</h4>";
                $render .= "<div class='hw-staff-phone'><a href='tel:$phone_international'>" . $member["phone"] . "</a></div>";
                if ($member['phone_alternate'] != null && $member['phone_alternate'] != "") {
                    $phone_alt_international = $this->convert_phone_format($member['phone_alternate']);
                    $render .= "<div class='hw-staff-phone'><a href='tel:$phone_alt_international'>" . $member["phone_alternate"] . "</a></div>";
                }
                $render .= "<div class='hw-staff-email'><a href='mailto:$email'>" . $email . "</a></div>";

                $render .= "</div>";
            }
            $render .= "</div>";

            return $render;
        }

        /**
         * Renders a column
         *
         * @param string $content Add content into this column
         * @param $column_index int index of the rendered column
         * @return string
         */
        private function render_column($content, $column_index)
        {
            $column_class = self::$column_class[self::$current_staff_element];

            if($column_index == 0) $column_class .= " first";

            $render = "<div class='$column_class'>";
            if ($content != null) {
                $render .= $content;
            }
            $render .= "</div>";

            return $render;
        }

        /**
         * Parses a single staff member item and saves the data to $staff_members
         *
         * @param $atts
         * @param string $content
         * @param string $shortcodename
         * @return string
         */
        function hw_staff_member_single($atts, $content = "", $shortcodename = "")
        {
            extract(shortcode_atts(array(
                'position' => "",
                'name' => "",
                'phone' => "",
                'email' => "",
                'phone_alternate' => "",
                'custom_markup' => ''
            ), $atts, 'av_testimonial_single'));

            /** @noinspection PhpUndefinedVariableInspection */
            $member = array(
                "position" => $position,
                "name" => $name,
                "phone" => $phone,
                "email" => $email,
                "phone_alternate" => $phone_alternate
            );
            $member_position = $member['position'];

            $staff = self::$staff_members[self::$current_staff_element];

            if ($staff[$member_position] == null) {
                $staff[$member_position] = array();
            }

            $member["index"] = $this->staff_member_count($staff);

            array_push($staff[$member_position], $member);

            self::$staff_members[self::$current_staff_element] = $staff;

            return "";
        }

        private function staff_member_count($staff)
        {
            $count = 0;
            foreach ($staff as $position => $members)
            {
                $count++;
            }
            return $count;
        }

        /**
         * Calculates position order sorted by mean indices
         *
         * @param array $staff The staff object
         * @return array The sorted positions
         */
        private function calculate_position_order($staff)
        {
            $positions = array(); // position => mean member index
            $mean_indices = array();

            foreach ($staff as $position => $members)
            {
                $mean_index = 0;
                foreach ($members as $i => $member)
                {
                    $mean_index += $member["index"];
                }
                $mean_index /= sizeof($members);

                array_push($positions, array(
                    "position" => $position,
                    "mean_index" => $mean_index
                ));

                array_push($mean_indices, $mean_index);
            }

            array_multisort($mean_indices, SORT_ASC, $positions);

            $sorted_positions = array();
            foreach ($positions as $index => $row)
            {
                array_push($sorted_positions, $row["position"]);
            }
            return $sorted_positions;
        }

    }


}
