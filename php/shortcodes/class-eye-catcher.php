<?php
/**
 * Copyright © 2017 seitenwind
 */

/**
 * Download shortcode
 *
 * Displays a download item
 */

if (!class_exists('EyeCatcher_Shortcode')) {

    class EyeCatcher_Shortcode extends aviaShortcodeTemplate
    {
        /**
         * Create the config array for the shortcode button
         */
        function shortcode_insert_button()
        {
            $this->config['name'] = __('Störer', 'avia_framework');
            $this->config['tab'] = __('Content Elements', 'avia_framework');
            $this->config['icon'] = get_stylesheet_directory_uri() . "/resources/images/icon-eye-catcher.png";
            $this->config['order'] = 131;
            $this->config['shortcode'] = 'hw-eye-catcher';
            $this->config['tooltip'] = __('Zeigt einen Störer an', 'avia_framework');
            $this->config['target'] = 'avia-target-insert';
            $this->config['tinyMCE'] = array('tiny_always' => true);
        }

        /**
         * Popup Elements
         *
         * If this function is defined in a child class the element automatically gets an edit button, that, when pressed
         * opens a modal window that allows to edit the element properties
         *
         * @return void
         */
        function popup_elements()
        {
            $this->elements = array(

                array(
                    "type" => "tab_container", 'nodescription' => true
                ),

                array(
                    "type" => "tab",
                    "name" => __("Content", 'avia_framework'),
                    'nodescription' => true
                ),

                array(
                    "name" 	=> __("Choose Image",'avia_framework' ),
                    "desc" 	=> __("Either upload a new, or choose an existing image from your media library",'avia_framework' ),
                    "id" 	=> "src",
                    "type" 	=> "image",
                    "title" => __("Insert Image",'avia_framework' ),
                    "button" => __("Insert",'avia_framework' ),
                    "std" 	=> AviaBuilder::$path['imagesURL']."placeholder.jpg"),

                array(
                    "name" => __("Überschrift", 'avia_framework'),
                    "desc" => __("Die Überschrift des Elements", 'avia_framework'),
                    "id" => "headline",
                    "type" => "input",
                    "std" => ""
                ),

                array(
                    "name" => __("Text", 'avia_framework'),
                    "desc" => __("Dieser Text wird unter der Überschrift angezeigt", 'avia_framework'),
                    "id" => "subtitle",
                    "type" => "input",
                    "std" => ""
                ),

                array(	"name" 	=> __("Button Label", 'avia_framework' ),
                    "desc" 	=> __("This is the text that appears on your button.", 'avia_framework' ),
                    "id" 	=> "buttonlabel",
                    "type" 	=> "input",
                    "std" => __("Click me", 'avia_framework' )),

                array(
                    "name" 	=> __("Button Link?", 'avia_framework' ),
                    "desc" 	=> __("Where should your button link to?", 'avia_framework' ),
                    "id" 	=> "buttonlink",
                    "type" 	=> "linkpicker",
                    "fetchTMPL"	=> true,
                    "subtype" => array(
                        __('Set Manually', 'avia_framework' ) =>'manually',
                        __('Single Entry', 'avia_framework' ) =>'single',
                        __('Taxonomy Overview Page',  'avia_framework' )=>'taxonomy',
                    ),
                    "std" 	=> ""),

                array(
                    "type" => "close_div",
                    'nodescription' => true
                ),

                array(
                    "type" => "close_div",
                    'nodescription' => true
                ),

            );
        }

        /**
         * Editor Element - this function defines the visual appearance of an element on the AviaBuilder Canvas
         * Most common usage is to define some markup in the $params['innerHtml'] which is then inserted into the drag and drop container
         * Less often used: $params['data'] to add data attributes, $params['class'] to modify the className
         *
         *
         * @param array $params this array holds the default values for $content and $args.
         * @return array $params the return array usually holds an innerHtml key that holds item specific markup.
         */
        function editor_element($params)
        {
            extract(av_backend_icon($params)); // creates $font and $display_char if the icon was passed as param "icon" and the font as "font"
            extract(shortcode_atts(array(
                'headline' => '',
                'subtitle' => '',
                'tag' => '',
                'src' => '',
                'buttonlabel' => '',
                'custom_class' => '',
            ), $params['args'], $this->config['shortcode']));

            $template = $this->update_template("src", "<img src='{{src}}' alt=''/>");
            $img	  = "";

            if(!empty($params['args']['attachment']) && !empty($params['args']['attachment_size']))
            {
                $img = wp_get_attachment_image($params['args']['attachment'],$params['args']['attachment_size']);
            }
            else if(isset($params['args']['src']) && is_numeric($params['args']['src']))
            {
                $img = wp_get_attachment_image($params['args']['src'],'large');
            }
            else if(!empty($params['args']['src']))
            {
                $img = "<img src='".$params['args']['src']."' alt=''  />";
            }

            $inner  = "<div class='avia_icon_element avia_textblock avia_textblock_style'>";
            $inner .= "    <div class='hw-eye-catcher-content-wrap'>";
            /** @noinspection PhpUndefinedVariableInspection */
            $inner .= "        <div class='avia_image_container hw-eye-catcher-image' {$template}>";
            $inner .= "            {$img}";
            $inner .= "        </div>";
            $inner .= "        <div class='hw-eye-catcher-text'>";
            /** @noinspection PhpUndefinedVariableInspection */
            $inner .= "            <h1 class='hw-eye-catcher-headline' data-update_with='headline'>$headline</h1>";
            /** @noinspection PhpUndefinedVariableInspection */
            $inner .= "            <p class='hw-eye-catcher-subtitle' data-update_with='subtitle'>$subtitle</p>";
            $inner .= "        </div>";
            $inner .= "        <div class='avia_button_box hw-eye-catcher-button'>";
            /** @noinspection PhpUndefinedVariableInspection */
            $inner .= "            <div class='avia-size-small' data-update_with='buttonlabel'>$buttonlabel</div>";
            $inner .= "        </div>";
            $inner .= "		</div>";

            $inner .= "</div>";

            $params['innerHtml'] = $inner;
            $params['class'] = "";

            return $params;
        }

        /**
         * Frontend Shortcode Handler
         *
         * @param array $atts array of attributes
         * @param string $content text within enclosing form of shortcode element
         * @param string $shortcodename the shortcode found, when == callback name
         * @return string $output returns the modified html string
         */
        function shortcode_handler($atts, $content = "", $shortcodename = "", $meta = "")
        {
            extract(shortcode_atts(array(
                'headline' => '',
                'subtitle' => '',
                'tag' => '',
                'src' => '',
                'buttonlabel' => '',
                'buttonlink' => '',
                'custom_class' => '',
            ), $atts, $this->config['shortcode']));

            $img = "";
            if(isset($atts['src']) && is_numeric($atts['src']))
            {
                $img = wp_get_attachment_image($atts['src'],'large');
            }
            else if(!empty($atts['src']))
            {
                $img = "<img src='".$atts['src']."' alt=''  />";
            }

            $buttonlink = explode(",", $buttonlink)[1];
            $buttonlink = get_page_link($buttonlink);

            $render = "";

            $render .= "<div class='hw-eye-catcher-container'>";
            $render .= "    <div class='hw-eye-catcher-content-wrap'>";
            $render .= "        <div class='hw-cell'>";
            $render .= "            <div class='hw-eye-catcher-text'>";
            $render .= "                <h1 class='hw-eye-catcher-headline'>$headline</h1>";
            $render .= "                <p class='hw-eye-catcher-subtitle'>$subtitle</p>";
            $render .= "            </div>";
            $render .= "        </div>";
            $render .= "        <div class='hw-cell'>";
            $render .= "            <a href='$buttonlink' class='hw-eye-catcher-button avia-button avia-icon_select-no avia-color-theme-color avia-size-small avia-position-center'>";
            $render .= "                <span class='avia_iconbox_title'>$buttonlabel</span>";
            $render .= "            </a>";
            $render .= "        </div>";
            $render .= "    </div>";
            $render .= "</div>";

            return $render;
        }
    }
}
