<?php
/**
 * Copyright © 2017 seitenwind
 */

add_action('init', 'create_post_type');

function create_post_type()
{
    register_post_type('hw_deal',
        array(
            'labels' => array(
                'name' => __('Angebote'),
                'singular_name' => __('Angebot')
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'deals'),
            'menu_position' => 5, // below posts
            'menu_icon' => get_stylesheet_directory_uri() . '/resources/images/icon-cpt-hw-deal.png'
        )
    );
}
