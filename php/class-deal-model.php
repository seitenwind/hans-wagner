<?php

/**
 * Copyright © 2017 seitenwind
 */
class Deal_Model
{

    /** @var WP_Post */
    private $post;

    /**
     * Deal_Model constructor.
     * @param WP_Post $post
     */
    public function __construct($post)
    {
        $this->post = $post;
    }

	/**
	 * Returns the post id
	 *
	 * @return int
	 */
    public function get_post_id()
    {
    	return $this->post->ID;
    }

    /**
     * Returns the title of the deal
     *
     * @return string
     */
    public function get_title()
    {
        return $this->post->post_title;
    }

    /**
     * Returns the image uri of the deal
     *
     * @return string
     */
    public function get_image_uri()
    {
        return wp_get_attachment_image_src($this->post->image, "full")[0];
    }

    /**
     * Returns the image attachment id
     *
     * @return int
     */
    public function get_image_id()
    {
        return $this->post->image;
    }

    /**
     * Returns the pdf uri of the deal
     *
     * @return string
     */
    public function get_pdf_uri()
    {
        return $this->post->pdf;
    }

}