<?php

/**
 * Copyright © 2017 seitenwind
 */
class Deals_Widget extends WP_Widget
{

    /**
     * Sets up the widgets name etc
     */
    public function __construct()
    {
        $widget_ops = array(
            'classname' => 'deals_widget',
            'description' => 'Zeigt die aktuellen Angebote an',
        );
        parent::__construct('deals_widget', 'Aktuelle Angebote', $widget_ops);
    }

    /**
     * Outputs the content of the widget
     *
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance)
    {
        ?>
        <div class="widget hw-deals-widget">
            <h3 class="widgettitle">Unsere Aktuellen Angebote</h3>
            <!--suppress HtmlUnknownTarget -->
            <img src="/wp-content/themes/hanswagner/resources/images/new-deals.png"
            <?php
            $this->render_button("Jetzt Informieren", "/sortiment");
            ?>
        </div>
        <?
    }

    /**
     * Renders a button
     *
     * @param string $label
     * @param string $href
     * @param string $size
     * @param string $position
     */
    private function render_button($label, $href = "#", $size = "small", $position = "center")
    {
        ?>
        <div class="avia-button-wrap avia-button-<? echo $position; ?> avia-builder-el-15 avia-builder-el-no-sibling  ">
            <a href="<? echo $href; ?>" class="avia-button avia-icon_select-no avia-color-theme-color avia-size-<? echo $size; ?> avia-position-<? echo $position; ?> ">
                <span class="avia_iconbox_title"><? echo $label ?></span>
            </a>
        </div>
        <?
    }

    /**
     * Outputs the options form on admin
     *
     * @param array $instance The widget options
     * @return string
     */
    public function form($instance)
    {
        $this->render_input_field($instance, "deal_count", "Anzahl an Angeboten", "");
        return "";
    }

    private function render_input_field($instance, $field_name, $label, $default_value, $type = "text")
    {
        $value = !empty($instance[$field_name]) ? $instance[$field_name] : esc_html__($default_value, 'hw_widgets');
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id($field_name)); ?>"><?php esc_attr_e($label . ':', 'text_domain'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id($field_name)); ?>" name="<?php echo esc_attr($this->get_field_name($field_name)); ?>" type="<?php echo $type ?>" value="<?php echo esc_attr($value); ?>">
        </p>
        <?
    }

    /**
     * Processing widget options on save
     *
     * @param array $new_instance The new options
     * @param array $old_instance The previous options
     *
     * @return array
     */
    public function update($new_instance, $old_instance)
    {
        return $new_instance;
    }

}