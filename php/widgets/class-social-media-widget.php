<?php

/**
 * Copyright © 2017 seitenwind
 */
class SocialMedia_Widget extends WP_Widget
{

    /**
     * Sets up the widgets name etc
     */
    public function __construct()
    {
        $widget_ops = array(
            'classname' => 'socialmedia_widget',
            'description' => 'Zeigt social media icons an',
        );
        parent::__construct('socialmedia_widget', 'Social Media Icons', $widget_ops);
    }

    /**
     * Outputs the content of the widget
     *
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance)
    {
        ?>
        <div class="widget social-media-widget">
        <?
            $this->render_frontend_link($instance, "facebook");
            $this->render_frontend_link($instance, "twitter");
            $this->render_frontend_link($instance, "email");
        ?>
        </div>
        <?
    }

    /**
     * Renders a frontend link
     *
     * @param $instance
     * @param $id
     */
    private function render_frontend_link($instance, $id)
    {
        if($instance[$id] == "") return;
        ?>
        <a target="_blank" class="social-media-link social-media-<? echo $id ?>" href="<? echo $instance[$id] ?>"></a>
        <?
    }

    /**
     * Outputs the options form on admin
     *
     * @param array $instance The widget options
     * @return string
     */
    public function form($instance)
    {
        $this->render_input_field($instance, "facebook", "Facebook", "");
        $this->render_input_field($instance, "twitter", "Twitter", "");
        $this->render_input_field($instance, "email", "E-Mail", "");

        return "";
    }

    private function render_input_field($instance, $field_name, $label, $default_value)
    {
        $value = !empty($instance[$field_name]) ? $instance[$field_name] : esc_html__($default_value, 'hw_widgets');
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id($field_name)); ?>"><?php esc_attr_e($label . ':', 'text_domain'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id($field_name)); ?>" name="<?php echo esc_attr($this->get_field_name($field_name)); ?>" type="text" value="<?php echo esc_attr($value); ?>">
        </p>
        <?
    }

    /**
     * Processing widget options on save
     *
     * @param array $new_instance The new options
     * @param array $old_instance The previous options
     *
     * @return array
     */
    public function update($new_instance, $old_instance)
    {
        return $new_instance;
    }

}