<?php

/**
 * Copyright © 2017 seitenwind
 */
class Util {

	public static function debug_var( $variable ) {
		echo "<pre class='debug'>";
		var_dump( $variable );
		echo "</pre>";
	}

	public static function is_ie_11() {
		return strpos( $_SERVER['HTTP_USER_AGENT'], 'rv:11.0' ) !== false && strpos( $_SERVER['HTTP_USER_AGENT'], 'Trident/7.0;' ) !== false;
	}

}
