<?php
/**
 * Copyright © 2017 seitenwind
 */

/**
 * Implements text ellipsis supporting primitive sentence and word modes.
 */
class Ellipsis {

	const MODE_SENTENCE = "mode_sentence";
	const MODE_TOKEN = "mode_token";
	const MODE_WORD = "mode_word";

	private $mode;
	private $length;
	private $append;

	/**
	 * HW_Ellipsis constructor.
	 *
	 * @param string $mode Use one of the constant modes
	 * @param number $length The length of the ellipsed text
	 * @param string $append The end of the ellipsed text
	 */
	public function __construct( $mode, $length, $append = "" ) {
		$this->mode   = $mode;
		$this->length = $length;
		$this->append = $append;
	}

	/**
	 * Ellipses the text
	 *
	 * @param $string
	 *
	 * @return string
	 */
	public function ellipse( $string ) {
		$atoms = null;

		switch ( $this->mode ) {
			case self::MODE_SENTENCE:
				$atoms = $this->split_sentences( $string );
				break;
			case self::MODE_WORD:
				$atoms = $this->split_words( $string );
				break;
			case self::MODE_TOKEN:
				$atoms = $this->split_tokens( $string );
				break;
			default:
				$atoms = $this->split_tokens( $string );
				break;
		}

		$result = "";

		foreach ( $atoms as $index => $atom ) {
			$result .= $atom[0] . $atom[1];
			if ( strlen( $result ) >= $this->length ) {
				break;
			}
		}

		return $result . $this->append;
	}

	/**
	 * Splits the string into sentences
	 *
	 * @param $string
	 *
	 * @return array
	 */
	private function split_sentences( $string ) {
		$atoms   = [];
		$pattern = "/\\!|\\?|\\.|:/";
		$matches = [];
		preg_match_all( $pattern, $string, $matches );

		$matches = $matches[0];

		foreach ( preg_split( $pattern, $string ) as $index => $sentence ) {
			$atoms[] = array( $sentence, $index < sizeof( $matches ) ? $matches[ $index ] : "" );
		}

		return $atoms;
	}

	/**
	 * Splits the string into words
	 *
	 * @param $string
	 *
	 * @return array
	 */
	private function split_words( $string ) {
		$atoms = [];
		foreach ( explode( " ", $string ) as $key => $word ) {
			$atoms[] = array( $word, " " );
		}
		$atoms[ sizeof( $atoms ) - 1 ][1] = "";

		return $atoms;
	}

	/**
	 * Splits the string into tokens
	 *
	 * @param $string
	 *
	 * @return array
	 */
	private function split_tokens( $string ) {
		$atoms = [];
		foreach ( str_split( $string ) as $key => $token ) {
			$atoms[] = array( $token, "" );
		}

		return $atoms;
	}

	/**
	 * @return string
	 */
	public function getMode() {
		return $this->mode;
	}

	/**
	 * @param string $mode
	 */
	public function setMode( $mode ) {
		$this->mode = $mode;
	}

	/**
	 * @return number
	 */
	public function getLength() {
		return $this->length;
	}

	/**
	 * @param number $length
	 */
	public function setLength( number $length ) {
		$this->length = $length;
	}

	/**
	 * @return string
	 */
	public function getAppend() {
		return $this->append;
	}

	/**
	 * @param string $append
	 */
	public function setAppend( string $append ) {
		$this->append = $append;
	}

}