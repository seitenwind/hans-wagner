<?

/**
 * Models data the custom post type "deals"
 *
 * Class Deals_Model
 */
class Deals_Model {

	/** @var Deals_Model */
	private static $instance;

	/** @var Deal_Model[] */
	private $deals;

	/**
	 * Deals_Model constructor.
	 */
	private function __construct() {
		self::$instance = $this;
	}

	/**
	 * Retrieves an instance of the deals model
	 *
	 * @return Deals_Model
	 */
	public static function get_instance() {
		return self::$instance == null ? new Deals_Model() : self::$instance;
	}

	/**
	 * Fetches all deals from the database
	 *
	 * @return Deal_Model[]
	 */
	public function fetch_all_deals() {
		return $this->fetch_deals( - 1 );
	}

	/**
	 * Fetches a deal by its id
	 *
	 * @param int $id
	 *
	 * @return Deal_Model
	 */
	public function fetch_deal_by_id( $id ) {
		return new Deal_Model( get_post( $id ) );
	}

	/**
	 * Fetches current deal entries from the database
	 *
	 * @param $deal_count int
	 *
	 * @return Deal_Model[]
	 */
	public function fetch_deals( $deal_count ) {
		$deal_posts = get_posts( array(
			'posts_per_page'   => $deal_count,
			'offset'           => 0,
			'category'         => '',
			'category_name'    => '',
			'orderby'          => 'date',
			'order'            => 'DESC',
			'include'          => '',
			'exclude'          => '',
			'meta_key'         => '',
			'meta_value'       => '',
			'post_type'        => 'hw_deal',
			'post_mime_type'   => '',
			'post_parent'      => '',
			'author'           => '',
			'author_name'      => '',
			'post_status'      => 'publish',
			'suppress_filters' => true
		) );

		$deals = array();
		foreach ( $deal_posts as $key => $deal_post ) {
			array_push( $deals, new Deal_Model( $deal_post ) );
		}

		return $deals;
	}

}