<?php
/**
 * Copyright © 2017 seitenwind
 */

/**
 * This class contains some static variables related to the enfold parent theme
 */
class Enfold
{
    public static $STYLE_LAYERSLIDER = "layerslider";
    public static $STYLE_LS_USER = "ls-user";
    public static $STYLE_LAYERSLIDER_GLOBAL = "layerslider-global";
    public static $STYLE_THICKBOX = "thickbox";
    public static $STYLE_WP_POINTER = "wp-pointer";
    public static $STYLE_DASHICONS = "dashicons";
    public static $STYLE_LAYERSLIDER_ADMIN = "layerslider-admin";
    public static $STYLE_LAYERSLIDER_ADMIN_NEW = "layerslider-admin-new";
    public static $STYLE_CODEMIRROR = "codemirror";
    public static $STYLE_CODEMIRROR_SOLARIZED = "codemirror-solarized";
    public static $STYLE_GOOGLE_FONTS_INDIE_FLOWER = "google-fonts-indie-flower";
    public static $STYLE_LAYERSLIDER_TR_GALLERY = "layerslider-tr-gallery";
    public static $STYLE_MINICOLOR = "minicolor";
    public static $STYLE_LS_SKIN_EDITOR = "ls-skin-editor";
    public static $STYLE_LS_GOOGLE_FONTS = "ls-google-fonts";
    public static $STYLE_AVIA_EVENTS_CAL = "avia-events-cal";
    public static $STYLE_AVIA_WPML = "avia-wpml";
    public static $STYLE_AVIA_WOOCOMMERCE_CSS = "avia-woocommerce-css";
    public static $STYLE_AVIA_ADMIN = "avia_admin";
    public static $STYLE_JQUERY_UI_DATEPICKER = "jquery-ui-datepicker";
    public static $STYLE_AVIA_WOOCOMMERCE_BOOKINGS_CSS = "avia-woocommerce-bookings-css";
    public static $STYLE_AVIA_MEDIA_STYLE = "avia-media-style";
    public static $STYLE_AVIA_GRAVITY = "avia-gravity";
    public static $STYLE_AVIA_GALLERY_MODE = "avia_gallery_mode";
    public static $STYLE_AVIA_BBPRESS = "avia-bbpress";
    public static $STYLE_FILENAME = "filename";
    public static $STYLE_AVIA_ADMIN_NEW = "avia_admin_new";
    public static $STYLE_PLUGIN_INSTALL = "plugin-install";
    public static $STYLE_AVIA_MODAL_STYLE = "avia-modal-style";
    public static $STYLE_AVIA_BUILDER_STYLE = "avia-builder-style";
    public static $STYLE_WP_COLOR_PICKER = "wp-color-picker";
    public static $STYLE_AVIA_SIDEBAR = "avia_sidebar";
    public static $STYLE_AVIA_GRID = "avia-grid";
    public static $STYLE_AVIA_BASE = "avia-base";
    public static $STYLE_AVIA_LAYOUT = "avia-layout";
    public static $STYLE_AVIA_SCS = "avia-scs";
    public static $STYLE_AVIA_POPUP_CSS = "avia-popup-css";
    public static $STYLE_AVIA_MEDIA = "avia-media";
    public static $STYLE_AVIA_PRINT = "avia-print";
    public static $STYLE_AVIA_RTL = "avia-rtl";
    public static $STYLE_AVIA_DYNAMIC = "avia-dynamic";
    public static $STYLE_AVIA_CUSTOM = "avia-custom";
    public static $STYLE_AVIA_STYLE = "avia-style";
}