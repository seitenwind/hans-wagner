/*
 * Copyright © 2017 seitenwind
 */

/**
 * This dummy replaces the original hyphen package when ES 6 is not available
 */
createHyphenator = function () {
    return function (text) {
        return text;
    }
};
