<?php
/**
 * Copyright © 2017 seitenwind
 */

require("php/class-setup.php");
require("php/class-enfold.php");
require("php/widgets/class-social-media-widget.php");
require("php/widgets/class-deals-widget.php");
require("php/class-deal-model.php");
require("php/class-deals-model.php");
require("php/class-ellipsis.php");
require("php/class-util.php");
require("php/custom-post-types/ctp-deals.php");

Setup::disable_feeds();
Setup::disable_loading_google_fonts();
Setup::register_shortcode_directory();
Setup::disable_automatic_linebreaks();
Setup::avia_enable_template_builder_custom_css();
Setup::load_widget("SocialMedia_Widget");
Setup::load_widget("Deals_Widget");
Setup::load_script("jquery-ui-core");
Setup::load_script("jquery-ui-widget");
Setup::load_script("jquery-ui-accordion");

// loads a dummy for the hyphen package because IE11 does not support ES 6 template strings
if(Util::is_ie_11())
{
	Setup::load_script("hyphen", "/js/hyphen-dummy-ie.min.js");
}
else
{
	Setup::load_script("hyphen", "/js/lib/hyphen/hyphen.js");
	Setup::load_script("hyphen-pattern-de", "/js/lib/hyphen/patterns/de.js", array("hyphen"));
}

Setup::load_script("hw-content-script", "/js/content-script.js", array("jquery-ui-accordion", "jquery", "hyphen-pattern-de"));

Setup::dequeue_enfold_style(Enfold::$STYLE_AVIA_BASE);
Setup::dequeue_enfold_style(Enfold::$STYLE_AVIA_DYNAMIC);
Setup::do_setup();

register_nav_menu("error-page", "Displays a Menu at the bottom of the error page");